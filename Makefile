
ifneq ($(CI_JOB_ID),)
DOCKER_TAG=$(CI_JOB_ID)
else
DOCKER_TAG=$(shell whoami)
endif


build:
	docker build -t $(CI_REGISTRY_IMAGE):$(DOCKER_TAG) .

push:
	docker push $(CI_REGISTRY_IMAGE):$(DOCKER_TAG)

tag-release:
	docker tag $(CI_REGISTRY_IMAGE):$(DOCKER_TAG) $(CI_REGISTRY_IMAGE):latest

push-release:
	docker push $(CI_REGISTRY_IMAGE):latest
